import { pc_rule } from "./PcRuleModel";

export class pc_day {
  public id_day: number;

  public txt_description: string;

  public pc_rule: pc_rule[];
}
