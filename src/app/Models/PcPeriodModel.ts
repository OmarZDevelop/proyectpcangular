import { pc_rule } from "./PcRuleModel";

export class pc_period {
  public id_period: number;

  public txt_description: string;

  public pc_rule: pc_rule[];
}
