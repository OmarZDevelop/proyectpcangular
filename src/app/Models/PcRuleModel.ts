import { Time } from "@angular/common";
import { pc_day } from "./PcDayModel";
import { pc_period } from "./PcPeriodModel";
import { pc_validator_digit } from "./PcValidadorDigitModel";

export class pc_rule {
  public id_rule: number;

  public id_day: number;

  public start_time: Time;

  public end_time: Time;

  public id_period: number;

  public pc_day: pc_day;

  public pc_period: pc_period;

  public pc_validator_digit: pc_validator_digit[];
}
