import { pc_rule } from "./PcRuleModel";

export class pc_validator_digit {
  public id_validator_digit: number;

  public digit_value: number;

  public id_rule: number;

  public pc_rule: pc_rule;
}
