import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ExternalServiceConfig } from "../ExternalServiceConfig";
import { ResponseModel } from "../Models/ResponseModel";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class GlobalService {
  constructor(private httpClient: HttpClient) {}
  onCheckCarEnable(
    num_placa: string,
    date: string,
    time: string
  ): Observable<ResponseModel> {
    return this.httpClient.get<ResponseModel>(
      ExternalServiceConfig.UrlDomine +
        "PcRule/isCarEnable?num_plate=" +
        num_placa +
        "&date=" +
        date +
        "&time=" +
        time
    );
  }
}
