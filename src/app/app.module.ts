import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { PicoPlacaQueryComponent } from "./pico-placa-query/pico-placa-query.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule,
  MAT_DATE_LOCALE
} from "@angular/material";
import { GlobalService } from "./Services/GlobalService";
import { HttpClientModule } from "@angular/common/http";
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [AppComponent, PicoPlacaQueryComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot() 
  ],
  providers: [GlobalService, { provide: MAT_DATE_LOCALE, useValue: "es-EC" }],
  bootstrap: [AppComponent]
})
export class AppModule {}
