import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicoPlacaQueryComponent } from './pico-placa-query.component';

describe('PicoPlacaQueryComponent', () => {
  let component: PicoPlacaQueryComponent;
  let fixture: ComponentFixture<PicoPlacaQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicoPlacaQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicoPlacaQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
