import { Component, OnInit } from "@angular/core";
import { NgModel } from "@angular/forms";
import { Time } from "@angular/common";
import { GlobalService } from "../Services/GlobalService";
import { ResponseModel } from "../Models/ResponseModel";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-pico-placa-query",
  templateUrl: "./pico-placa-query.component.html",
  styleUrls: ["./pico-placa-query.component.css"]
})
export class PicoPlacaQueryComponent implements OnInit {
  num_placa: string;
  date: Date;
  time: Time;
  constructor(
    private globalService: GlobalService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {}
  onSend() {
    console.log(this.date);
    if (this.num_placa == "") {
      alert("Número de placa es obligatorio");
    } else if (this.date == null) {
      alert("Fecha es obligatorio");
    } else if (this.time == null) {
      alert("Hora es  obligatorio");
    } else {
      this.globalService
        .onCheckCarEnable(
          this.num_placa,
          this.date.toISOString(),
          this.time.toString()
        )
        .subscribe((res: ResponseModel) => {
          if (res.error == false) {
            this.toastr.info(res.description, "Información", {
              timeOut: 5000
            });
          } else {
            this.toastr.error(res.description, "Error", {
              timeOut: 5000
            });
          }
        });
    }
  }
}
